λ set NUGET_API_KEY=...

cd src
dotnet build -c Release
dotnet nuget push bin\Release\Tostil.Tools.Queues.1.2.0.nupkg -k "%NUGET_API_KEY%" -s https://api.nuget.org/v3/index.json
