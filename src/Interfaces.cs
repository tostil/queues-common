﻿namespace Tostil.Tools.Queues
{
    public interface IQueueMessage
    {
        string Content { get; set; }
        bool IsEmpty { get; }
    }

    public interface IQueue
    {
        string Name { get; set; }
    }

    public interface IQueueAdapter
    {
        QueueAdapterSettings Settings { get; }
        IQueue Queue { get; }

        bool Push(IQueueMessage item);
        IQueueMessage Pop();
        IQueueMessage Get();
        bool Delete(IQueueMessage item);
        void Dispose();
    }

}
