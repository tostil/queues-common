﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Tostil.Tools.Queues
{
    [Serializable]
    public class QueueMessageBase : IQueueMessage
    {
        #region private members        
        protected string _content;   
  
        #endregion

        #region public fields
        
        public virtual string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        public bool IsEmpty
        {
            get
            {
                return !(_content != null && !string.IsNullOrEmpty(_content));
            }
        }
        #endregion

        #region constructors
        public QueueMessageBase()
        { }

        public QueueMessageBase(string data)
        {         
            _content = data;
        }
        #endregion

        #region public methods
        public virtual byte[] AsBytes()
        {
            return Encoding.UTF8.GetBytes(_content);
        }

        public virtual string AsString(Encoding encoding)
        {
            return _content;
        }
        
        public virtual Stream AsStream()
        {
            return new MemoryStream(AsBytes());
        }
        #endregion

    }
}
