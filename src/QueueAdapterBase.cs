﻿using Microsoft.Extensions.Options;
using System;

namespace Tostil.Tools.Queues
{
    [Serializable]
    public abstract class QueueAdapterBase : IQueueAdapter , IDisposable
    {
        #region Private Membes
        private const string DATA_CONNECTION_STRING_KEY_NAME = "Tostil.Tools.Queues.DataConnectionString";
        protected QueueAdapterSettings _settings;
        protected IQueue _queue;
        #endregion

        #region Public Members
        public QueueAdapterSettings Settings 
        {
            get 
            {
                return _settings;
            }

            set
            {
                _settings = value;
            }
        }

        public IQueue Queue
        {
            get
            {
                return _queue;
            }
            set
            {
                _queue = value;
            }
        }
        #endregion

        #region Constructors
        public QueueAdapterBase(IOptions<QueueAdapterSettings> settings)
        {
            if (settings != null && settings.Value != null)
            {
                _settings = settings.Value;
            }
            else
            {
                _settings = new QueueAdapterSettings();
            }
        }

        public virtual void Dispose()
        {
            _queue = null;
        }
        #endregion

        #region Private Methods


        private void CheckDataConnectionStringAndQueue()
        {
            if (string.IsNullOrEmpty(_settings.DataConnectionString))
            {
                throw new Exception("DataConnectionString not initialized");
            }            
        }
        #endregion

        #region Push
        public bool Push(IQueueMessage item)
        {
            CheckDataConnectionStringAndQueue();
            return PushInternal(item);
        }

        protected abstract bool PushInternal(IQueueMessage item);
        #endregion

        #region Pop and Get
        public IQueueMessage Pop()
        {
            CheckDataConnectionStringAndQueue();
            return PopInternal();
        }

        protected abstract IQueueMessage PopInternal();

        public IQueueMessage Get()
        {
            CheckDataConnectionStringAndQueue();
            return GetInternal();
        }

        protected abstract IQueueMessage GetInternal();
        #endregion

        #region Delete
        public bool Delete(IQueueMessage item)
        {
            CheckDataConnectionStringAndQueue();
            return DeleteInternal(item);
        }

        protected abstract bool DeleteInternal(IQueueMessage item);
        #endregion
    }
}
