﻿using System;

namespace Tostil.Tools.Queues
{
    [Serializable]
    public class QueueBase : IQueue
    {
        #region private members
        protected string _name;
        protected bool _isPrivate;
        #endregion

        #region public members
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }        

        public bool IsPrivate
        {
            get { return _isPrivate; }
            set { _isPrivate = value; }
        }
        #endregion

        #region Constructors
        public QueueBase(string name, bool isPrivate)
        {
            _name = name;
            _isPrivate = isPrivate;
        }

        public QueueBase(string name) 
            : this(name, true)
        {
        }
        #endregion
    }
}
